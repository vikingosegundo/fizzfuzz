//
//  FizzFuzzRangeCheckerTests.swift
//  fizzfuzztests
//
//  Created by manuel on 21/12/2018.
//  Copyright © 2018 tweede viking. All rights reserved.
//

import XCTest

class FizzFuzzRangeCheckerTests: XCTestCase {
    var sut: FizzFuzzRangeChecker!
    override func setUp()    { sut = FizzFuzzRangeChecker() }
    override func tearDown() { sut = nil }
    func testFizzFuzzRangeChecker() {
        XCTAssertTrue(sut.check(from: 1, to:  3).count == 3)
        XCTAssertTrue(sut.check(from: 1, to:  3) == [.no(1), .no(2), .fizz])
        XCTAssertTrue(sut.check(from: 5, to:  9) == [.fuzz, .fizz, .no(7), .no(8),.fizz])
        XCTAssertTrue(sut.check(from:13, to: 16) == [.no(13), .no(14),.fizzFuzz, .no(16)])
    }
}
