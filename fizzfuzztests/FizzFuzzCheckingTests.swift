//
//  fizzfuzztests.swift
//  fizzfuzztests
//
//  Created by manuel on 21/12/2018.
//  Copyright © 2018 tweede viking. All rights reserved.
//

import XCTest

class FizzFuzzCheckingTests: XCTestCase {
    var sut: FizzFuzzChecker!
    override func setUp()    { sut = FizzFuzzChecker() }
    override func tearDown() { sut = nil               }
    func testCecksOneCorrectly() {
        XCTAssertTrue(sut.fizzOrFuzz(for:1) != .fizz)
        XCTAssertTrue(sut.fizzOrFuzz(for:1) != .fuzz)
        XCTAssertTrue(sut.fizzOrFuzz(for:1) == .no(1))
        XCTAssertTrue(sut.fizzOrFuzz(for:1) != .fizzFuzz)
    }
    func testCecksThreeCorrectly() {
        XCTAssertTrue(sut.fizzOrFuzz(for:3) == .fizz)
        XCTAssertTrue(sut.fizzOrFuzz(for:3) != .fuzz)
        XCTAssertTrue(sut.fizzOrFuzz(for:3) != .no(3))
        XCTAssertTrue(sut.fizzOrFuzz(for:3) != .fizzFuzz)
    }
    func testCecksFiveCorrectly() {
        XCTAssertTrue(sut.fizzOrFuzz(for:5) != .fizz)
        XCTAssertTrue(sut.fizzOrFuzz(for:5) == .fuzz)
        XCTAssertTrue(sut.fizzOrFuzz(for:5) != .no(5))
        XCTAssertTrue(sut.fizzOrFuzz(for:5) != .fizzFuzz)
    }
    func testCecks15Correctly() {
        XCTAssertTrue(sut.fizzOrFuzz(for:15) != .fizz)
        XCTAssertTrue(sut.fizzOrFuzz(for:15) != .fuzz)
        XCTAssertTrue(sut.fizzOrFuzz(for: 5) != .no(15))
        XCTAssertTrue(sut.fizzOrFuzz(for:15) == .fizzFuzz)
    }
}
