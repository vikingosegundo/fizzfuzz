//
//  FizzFuzzChecking.swift
//  fizzfuzz
//
//  Created by manuel on 21/12/2018.
//  Copyright © 2018 tweede viking. All rights reserved.
//

import Foundation

enum FizzFuzz {
    case fizz
    case fuzz
    case fizzFuzz
    case no(Int)
}
extension FizzFuzz:Equatable { }
extension FizzFuzz:CustomStringConvertible {
    var description:String {
        switch self {
        case .fizz     : return "fizz"
        case     .fuzz : return "fuzz"
        case .fizzFuzz : return "fizzfuzz"
        case let .no(n): return "\(n)"
        }
    }
}
struct FizzFuzzChecker {
    private func checkDividableBy3(_ n: Int) -> Bool { n % 3 == 0 }
    private func checkDividableBy5(_ n: Int) -> Bool { n % 5 == 0 }

    func fizzOrFuzz(for number: Int) -> FizzFuzz {
        switch (checkDividableBy3(number), checkDividableBy5(number)) {
        case (false,false): return .no(number)
        case ( true,false): return .fizz
        case (false, true): return .fuzz
        case ( true, true): return .fizzFuzz
        }
    }
}
struct FizzFuzzRangeChecker {
    private let checker = FizzFuzzChecker()
    func check(from:Int,to:Int) -> [FizzFuzz] {
        (from...to).map { checker.fizzOrFuzz(for:$0) }
    }
}
